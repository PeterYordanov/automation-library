﻿using OpenQA.Selenium.Appium.Windows;

namespace AutomationLibrary.Extensions
{
    public static class WinAppDriver
    {
        public static WindowsElement FindElementByAbsoluteXPath(this WindowsDriver<WindowsElement> desktopSession, string xPath, int tryCount = 15)
        {
            WindowsElement uiTarget = null;

            while (tryCount-- > 0)
            {
                uiTarget = desktopSession.FindElementByXPath(xPath);

                if (uiTarget != null)
                {
                    break;
                }
                else
                {
                    System.Threading.Thread.Sleep(2000);
                }
            }

            return uiTarget;
        }

        //public static WindowsElement FindElement(this WindowsElement element, string selector, By by)
        //{
        //    return by switch
        //    {
        //        By.AccessibilityId => element.FindElementByAccessibilityId(selector),
        //        By.ClassName => element.FindElementByClassName(selector),
        //        By.TagName => element.FindElementByTagName(selector),
        //        By.XPath => element.FindElementByXPath(selector),
        //        By.Id => element.FindElementById(selector),
        //        By.WindowsUIAutomation => element.FindElementByWindowsUIAutomation(selector),
        //        By.CssSelector => element.FindElementByCssSelector(selector),
        //        By.LinkText => element.FindElementByLinkText(selector),
        //        By.PartialLinkText => element.FindElementByPartialLinkText(selector),
        //        By.Name => element.FindElementByName(selector),
        //        //By.AbsoluteXPath => element.FindElementByAbsoluteXPath(selector),
        //        _ => throw new Exception("No such element type"),
        //    };
        //}
    }
}
