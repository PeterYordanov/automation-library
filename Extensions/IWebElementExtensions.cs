﻿using OpenQA.Selenium;

namespace AutomationLibrary.Extensions
{
    public static class IWebElementExtensions
    {
        public static void ClearAndSendKeys(this IWebElement webElement, string text)
        {
            if (!string.IsNullOrEmpty(text))
            {
                webElement.Clear();
                webElement.SendKeys(text);
            }
        }
    }
}
