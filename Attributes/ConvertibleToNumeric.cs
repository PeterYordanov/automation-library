﻿using System.ComponentModel.DataAnnotations;

namespace AutomationLibrary.Attributes
{
    public sealed class ConvertibleToDoubleAttribute : ValidationAttribute
    {
        public override bool IsValid(object value)
        {
            return double.TryParse(value.ToString(), out double number);
        }
    }

    public sealed class ConvertibleToIntAttribute : ValidationAttribute
    {
        public override bool IsValid(object value)
        {
            return int.TryParse(value.ToString(), out int number);
        }
    }

    public sealed class ConvertibleToFloatAttribute : ValidationAttribute
    {
        public override bool IsValid(object value)
        {
            return float.TryParse(value.ToString(), out float number);
        }
    }

    public sealed class ConvertibleToLongAttribute : ValidationAttribute
    {
        public override bool IsValid(object value)
        {
            return long.TryParse(value.ToString(), out long number);
        }
    }
}
