﻿namespace AutomationLibrary.TestUtilities
{
    public static class PageGenerator
    {
        public static T GetPage<T>() where T : new()
        {
            var page = new T();
            return page;
        }
    }
}
