﻿using TBI.SharedLibrary.Logging;
using AutomationLibrary.Enumerations;

namespace AutomationLibrary.Helpers
{
    public class LogModelExtended : LogModel
    {
        public EventType EventType { get; set; }
        public string InnerException { get; set; }
        public object Object { get; set; }
    }

}
