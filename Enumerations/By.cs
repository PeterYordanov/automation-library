﻿namespace AutomationLibrary.Enumerations.WinAppDriver
{
    public enum By
    {
        ClassName,
        TagName,
        XPath,
        AbsoluteXPath,
        Id,
        WindowsUIAutomation,
        CssSelector,
        AccessibilityId,
        LinkText,
        PartialLinkText,
        Name
    }
}
