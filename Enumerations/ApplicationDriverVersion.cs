﻿namespace AutomationLibrary.Enumerations
{
    public enum ApplicationDriverVersion
    {
        MSAA,
        UIA2,
        UIA3
    }
}
