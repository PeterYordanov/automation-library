﻿namespace AutomationLibrary.Enumerations
{
    public enum Browsers
    {
        Chrome, 
        InternetExplorer, 
        Firefox,
        Edge,
        PhantomJS
    }
}
