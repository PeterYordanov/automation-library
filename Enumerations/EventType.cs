﻿namespace AutomationLibrary.Enumerations
{
    public enum EventType
    {
        Successful,
        SuccessfulInput,
        ErrorInput,
        Error,
        Redirected,
        Exception,
        General
    }
}
