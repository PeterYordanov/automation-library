﻿using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.IE;
using System;
using AutomationLibrary.Enumerations;

namespace AutomationLibrary.Infrastructure
{
	public static class WebDriverFactory
	{
		public static IWebDriver CreateInstance(Browsers browser)
		{
            return browser switch
            {
                Browsers.Chrome => new ChromeDriver(),
                Browsers.Firefox => new FirefoxDriver(),
                Browsers.InternetExplorer => new InternetExplorerDriver(),
                _ => throw new ArgumentException("No driver with the provided browser type"),
            };
        }
	}
}
