﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using AutomationLibrary.Enumerations;

namespace AutomationLibrary.Infrastructure
{
    public class WebDriverFacade
    {
        private readonly IWebDriver webDriver = null;

        public WebDriverFacade(Browsers browser)
        {
            webDriver = WebDriverFactory.CreateInstance(browser);
        }

        public WebDriverFacade(IWebDriver webDriver)
        {
            this.webDriver = webDriver;
        }

        public IWebDriver GetDriver()
        {
            return webDriver;
        }

        public IWebElement FindShadowRootElement(string[] selectors)
        {
            IWebElement root = null;

            foreach (var selector in selectors)
            {
                root = (IWebElement)((IJavaScriptExecutor)webDriver).ExecuteScript("return arguments[0].querySelector(arguments[1]).shadowRoot", root, selector);
            }

            return root;
        }

        public IWebElement ExpandRootElement(IWebElement webElement)
        {
            return (IWebElement)((IJavaScriptExecutor)webDriver).ExecuteScript("return arguments[0].shadowRoot", webElement);
        }

        public void GoToUrl(string url)
        {
            webDriver.Url = url;
            webDriver.Navigate().GoToUrl(url);
            webDriver.Manage().Window.Maximize();
        }

        public void Manage(double seconds)
        {
            webDriver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(seconds);
        }

        public void SwitchToFrame(string frameId)
        {
            webDriver.SwitchTo().Frame(frameId);
        }

        public void SwitchToFrame(IWebElement frame)
        {
            webDriver.SwitchTo().Frame(frame);
        }

        public void SwitchToParentFrame()
        {
            webDriver.SwitchTo().ParentFrame();
        }

        public bool Contains(string value)
        {
            return webDriver.PageSource.Contains(value);
        }

        public void Quit()
        {
            webDriver.Quit();
            //webDriver.Close();
            //webDriver.Dispose();
        }

        public object ExecuteJavaScript(string script)
        {
            return ((IJavaScriptExecutor)webDriver).
                ExecuteScript("return " + script);
        }

        public IWebElement FindElement(By by)
        {
            IWebElement element = webDriver.FindElement(by);
            if (element == null)
            {
                return NullWebElement.NULL;
            }
            else
            {
                return element;
            }
        }

        public bool IsElementPresent(By by)
        {
            try
            {
                webDriver.FindElement(by);
                return true;
            }
            catch (NoSuchElementException)
            {
                return false;
            }
        }

        public bool IsElementEnabled(By by)
        {
            try 
            {
                if(webDriver.FindElement(by).Enabled)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            } 
            catch (Exception)
            {
                return false;
            }
        }

        public bool IsElementDisplayed(By by)
        {
            try
            {
                if (webDriver.FindElement(by).Displayed)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception)
            {
                return false;
            }
        }

        public bool IsAlertPresent()
        {
            try
            {
                webDriver.SwitchTo().Alert();
                return true;
            }
            catch (NoAlertPresentException)
            {
                return false;
            }
        }

        public void WaitForPageToBeFullyLoaded(int timeSpan)
        {
            new WebDriverWait(webDriver, TimeSpan.FromSeconds(timeSpan)).Until(
                d => ((IJavaScriptExecutor)d).ExecuteScript("return document.readyState").Equals("complete"));
        }

        public IWebElement WaitForCondition(Func<IWebDriver, IWebElement> expectedCondition, double timeout)
        {
            var webDriverWait = new WebDriverWait(webDriver, TimeSpan.FromSeconds(timeout));

            return webDriverWait.Until(expectedCondition);
        }

        public List<string> GetTabs()
        {
            string[] tabs = new string[webDriver.WindowHandles.Count];
            webDriver.WindowHandles.CopyTo(tabs, 0);
            return new List<string>(tabs);
        }

        public string GetTabAt(int index)
        {
            return GetTabs().ElementAt(index);
        }

        public void CloseTabAt(int index)
        {
            webDriver.SwitchTo().Window(GetTabAt(index)).Close();
        }

        public string DismissAlert()
        {
            IAlert alert = webDriver.SwitchTo().Alert();
            string alertText = alert.Text;
            alert.Dismiss();
            return alertText;
        }

        public string AcceptAlert()
        {
            IAlert alert = webDriver.SwitchTo().Alert();
            string alertText = alert.Text;
            alert.Accept();
            return alertText;
        }

        public void SwitchToTab(int index)
        {
            webDriver.SwitchTo().Window(GetTabAt(index));
        }

        public void GoBack()
        {
            webDriver.Navigate().Back();
        }

        public void GoForward()
        {
            webDriver.Navigate().Forward();
        }

        public void Refresh()
        {
            webDriver.Navigate().Refresh();
        }

        public Bitmap TakeScreenshot(By by)
        {
            var screenshotDriver = webDriver as ITakesScreenshot;
            Screenshot screenshot = screenshotDriver.GetScreenshot();
            var bmpScreen = new Bitmap(new MemoryStream(screenshot.AsByteArray));

            IWebElement element = FindElement(by);
            var cropArea = new Rectangle(element.Location, element.Size);
            return bmpScreen.Clone(cropArea, bmpScreen.PixelFormat);
        }

        public MemoryStream TakeScreenshot()
        {
            var screenshotDriver = webDriver as ITakesScreenshot;
            Screenshot screenshot = screenshotDriver.GetScreenshot();
            return new MemoryStream(screenshot.AsByteArray);
        }
    }
}
