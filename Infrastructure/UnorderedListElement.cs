﻿using OpenQA.Selenium;
using OpenQA.Selenium.Internal;
using System.Collections.ObjectModel;

namespace AutomationLibrary.Infrastructure
{
    public class UnorderedListElement : IWrapsElement
    {
        private ReadOnlyCollection<IWebElement> LinkElements { get; set; }
        public IWebElement WrappedElement { get; set; }

        public UnorderedListElement(IWebElement element)
        {
            LinkElements = element.FindElements(By.TagName("li"));
            WrappedElement = element;
        }

        public void SelectByText(string text)
        {
            foreach (var item in LinkElements)
            {
                if (item.Text.Trim().Equals(text) || item.Text.Trim().Contains(text.Trim()))
                {
                    item.Click();
                }
            }
        }
    }
}
