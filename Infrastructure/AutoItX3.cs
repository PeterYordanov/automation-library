﻿/*using System.Threading;

namespace AutomationLibrary.Infrastructure
{
    public class AutoItX3
    {
        public AutoItX3Lib.AutoItX3 autoIt;

        public AutoItX3()
        {
            autoIt = new AutoItX3Lib.AutoItX3();
        }

        public string LastWindow { get; set; } = "";

        private void LastWindowCheck(string win)
        {
            LastWindow = win != LastWindow ? LastWindow = win : LastWindow = LastWindow;
        }

        public bool Exists(string win)
        {
            LastWindowCheck(win);
            return autoIt.WinExists(win, "") != 0;
        }

        public bool Exists(string win, out string lastWindow)
        {
            lastWindow = win != LastWindow ? LastWindow = win : LastWindow;
            return autoIt.WinExists(win, "") != 0;
        }

        public bool Exists(string win, string txt)
        {
            LastWindowCheck(win);
            return autoIt.WinExists(win, txt) != 0;
        }

        public bool Exist(string win, string txt, out string lastWindow)
        {
            lastWindow = win != LastWindow ? LastWindow = win : LastWindow = LastWindow;
            return autoIt.WinExists(win, txt) != 0;
        }

        public bool Run(string exe, string dir)
        {
            return autoIt.Run(exe, dir, autoIt.SW_SHOW) != 0;
        }

        public bool RunWait(string exe, string dir)
        {
            return autoIt.Run(exe, dir, autoIt.SW_SHOW) != 0;
        }

        public void Sleep(int ms)
        {
            Thread.Sleep(ms);
        }

        public bool IsActive(string win)
        {
            LastWindowCheck(win);
            return autoIt.WinActive(win, "") != 0;
        }

        public bool IsActive(string win, string txt)
        {
            LastWindowCheck(win);
            return autoIt.WinActive(win, txt) != 0;
        }

        public bool IsActive(string win, out string lastWindow)
        {
            lastWindow = win != LastWindow ? LastWindow = win : LastWindow = LastWindow;
            return autoIt.WinActive(win, "") != 0;
        }

        public void Activate()
        {
            autoIt.WinActivate(LastWindow, "");
        }

        public void Activate(string win)
        {
            LastWindowCheck(win);
            autoIt.WinActivate(win, "");
        }

        public void Activate(string win, string txt)
        {
            LastWindowCheck(win);
            autoIt.WinActivate(win, txt);
        }

        public bool WaitNotActive()
        {
            return autoIt.WinWaitNotActive(LastWindow, "", 100) != 0;
        }

        public bool WaitNotActive(string win)
        {
            LastWindowCheck(win);
            return autoIt.WinWaitNotActive(win, "", 100) != 0;
        }

        public bool WaitNotActive(string win, int timeout)
        {
            LastWindowCheck(win);
            return autoIt.WinWaitNotActive(win, "", timeout) != 0;
        }

        public bool WaitNotActive(string win, string txt, int timeout)
        {
            LastWindowCheck(win);
            return autoIt.WinWaitNotActive(win, txt, timeout) != 0;
        }

        public bool WaitActive()
        {
            return autoIt.WinWaitActive(LastWindow, "", 100) != 0;
        }

        public bool WaitActive(string win)
        {
            LastWindowCheck(win);
            return autoIt.WinWaitActive(win, "", 100) != 0;
        }

        public bool WaitActive(string win, string txt)
        {
            LastWindowCheck(win);
            return autoIt.WinWaitActive(win, txt, 100) != 0;
        }

        public bool WaitActive(string win, string txt, int timeout)
        {
            LastWindowCheck(win);
            return autoIt.WinWaitActive(win, txt, timeout) != 0;
        }

        public void Close()
        {
            autoIt.WinClose(LastWindow, "");
        }

        public void Close(string win)
        {
            LastWindowCheck(win);
            autoIt.WinClose(win, "");
        }

        public void Close(string win, string txt)
        {
            LastWindowCheck(win);
            autoIt.WinClose(win, txt);
        }


        public string GetClassList()
        {
            return autoIt.WinGetClassList(LastWindow, "");
        }

        public string GetClassList(string win)
        {
            LastWindowCheck(win);
            return autoIt.WinGetClassList(win, "");
        }

        public string GetClassList(string win, string txt)
        {
            LastWindowCheck(win);
            return autoIt.WinGetClassList(win, txt);
        }

        public string GetWindowHandle()
        {
            return autoIt.WinGetHandle(LastWindow, "");
        }

        public string GetWindowHandle(string win)
        {
            LastWindowCheck(win);
            return autoIt.WinGetHandle(win, "");
        }

        public string GetWindowHandle(string win, string txt)
        {
            LastWindowCheck(win);
            return autoIt.WinGetHandle(win, txt);
        }

        public string GetProcess()
        {
            return autoIt.WinGetProcess(LastWindow, "");
        }

        public string GetProcess(string win)
        {
            LastWindowCheck(win);
            return autoIt.WinGetProcess(win, "");
        }

        public string GetProcess(string win, string txt)
        {
            LastWindowCheck(win);
            return autoIt.WinGetProcess(win, txt);
        }

        public int GetPosX()
        {
            return autoIt.WinGetPosX(LastWindow, "");
        }

        public int GetPosX(string win)
        {
            LastWindowCheck(win);
            return autoIt.WinGetPosX(win, "");
        }

        public int GetPosX(string win, string txt)
        {
            LastWindowCheck(win);
            return autoIt.WinGetPosX(win, txt);
        }

        public int GetPosY()
        {
            return autoIt.WinGetPosY(LastWindow, "");
        }

        public int GetPosY(string win)
        {
            LastWindowCheck(win);
            return autoIt.WinGetPosX(win, "");
        }

        public int GetPosY(string win, string txt)
        {
            LastWindowCheck(win);
            return autoIt.WinGetPosY(win, txt);
        }


        public int GetPosHeight()
        {
            return autoIt.WinGetPosHeight(LastWindow, "");
        }

        public int GetPosHeight(string win)
        {
            LastWindowCheck(win);
            return autoIt.WinGetPosX(win, "");
        }

        public int GetPosHeight(string win, string txt)
        {
            LastWindowCheck(win);
            return autoIt.WinGetPosHeight(win, txt);
        }

        public int GetPosWidth()
        {
            return autoIt.WinGetPosWidth(LastWindow, "");
        }

        public int GetPosWidth(string win)
        {
            LastWindowCheck(win);
            return autoIt.WinGetPosX(win, "");
        }

        public int GetPosWidth(string win, string txt)
        {
            LastWindowCheck(win);
            return autoIt.WinGetPosWidth(win, txt);
        }

        public int GetClientSizeHeight()
        {
            return autoIt.WinGetClientSizeHeight(LastWindow, "");
        }

        public int GetClientSizeHeight(string win)
        {
            LastWindowCheck(win);
            return autoIt.WinGetPosX(win, "");
        }

        public int GetClientSizeHeight(string win, string txt)
        {
            LastWindowCheck(win);
            return autoIt.WinGetClientSizeHeight(win, txt);
        }

        public int GetClientSizeWidth()
        {
            return autoIt.WinGetClientSizeWidth(LastWindow, "");
        }

        public int GetClientSizeWidth(string win)
        {
            LastWindowCheck(win);
            return autoIt.WinGetPosX(win, "");
        }

        public int GetClientSizeWidth(string win, string txt)
        {
            LastWindowCheck(win);
            return autoIt.WinGetClientSizeWidth(win, txt);
        }

        public string GetText()
        {
            return autoIt.WinGetText(LastWindow, "");
        }

        public string GetText(string win)
        {
            LastWindowCheck(win);
            return autoIt.WinGetText(win, "");
        }

        public string GetText(string win, string txt)
        {
            LastWindowCheck(win);
            return autoIt.WinGetText(win, txt);
        }

        public int GetState()
        {
            return autoIt.WinGetState(LastWindow, "");
        }

        public int GetState(string win)
        {
            LastWindowCheck(win);
            return autoIt.WinGetPosX(win, "");
        }

        public int GetState(string win, string txt)
        {
            LastWindowCheck(win);
            return autoIt.WinGetState(win, txt);
        }

        public void Kill()
        {
            autoIt.WinKill(LastWindow, "");
        }

        public void Kill(string win)
        {
            LastWindowCheck(win);
            autoIt.WinKill(win, "");
        }

        public void Kill(string win, string txt)
        {
            LastWindowCheck(win);
            autoIt.WinKill(win, txt);
        }

        public void Move(int x, int y)
        {
            autoIt.WinMove(LastWindow, "", x, y, -1, -1);
        }

        public void Move(string win, int x, int y)
        {
            LastWindowCheck(win);
            autoIt.WinMove(win, "", x, y, -1, -1);
        }

        public void Move(string win, string txt, int x, int y)
        {
            LastWindowCheck(win);
            autoIt.WinMove(win, txt, x, y, -1, -1);
        }

        public void Move(string win, string txt, int x, int y, out bool success)
        {
            LastWindowCheck(win);
            autoIt.WinMove(win, txt, x, y, -1, -1);
            success = autoIt.WinMove(win, txt, x, y, -1, -1) != 0;
        }

        public void Move(string win, int x, int y, out bool success)
        {
            LastWindowCheck(win);
            autoIt.WinMove(win, "", x, y, -1, -1);
            success = autoIt.WinMove(win, "", x, y, -1, -1) != 0;
        }

        public void Move(int x, int y, out bool success)
        {
            autoIt.WinMove(LastWindow, "", x, y, -1, -1);
            success = autoIt.WinMove(LastWindow, "", x, y, -1, -1) != 0;
        }

        public bool SetOnTop()
        {
            return autoIt.WinSetOnTop(LastWindow, "", 1) != 0;
        }

        public bool SetOnTop(string win)
        {
            LastWindowCheck(win);
            return autoIt.WinSetOnTop(win, "", 1) != 0;
        }

        public bool SetOnTop(string win, string txt)
        {
            LastWindowCheck(win);
            return autoIt.WinSetOnTop(win, txt, 1) != 0;
        }

        public bool SetOnTop(string win, int flag)
        {
            LastWindowCheck(win);
            if (flag != 0 || flag != 1)
            {
                return false;
            }
            return autoIt.WinSetOnTop(win, "", flag) != 0;
        }

        public bool SetOnTop(string win, string txt, int flag)
        {
            LastWindowCheck(win);
            if (flag != 0 || flag != 1)
            {
                return false;
            }
            return autoIt.WinSetOnTop(win, txt, flag) != 0;
        }
    }
}
*/