﻿using OpenQA.Selenium.Appium;
using OpenQA.Selenium.Appium.Android;
using OpenQA.Selenium.Appium.iOS;
using OpenQA.Selenium.Appium.Windows;
using System;

namespace AutomationLibrary.Infrastructure
{
    public static class WinAppDriverContext
    {
        private readonly static AppiumOptions appiumOptions = new AppiumOptions();

        public static void AddCapability(string name, string value)
        { 
            appiumOptions.AddAdditionalCapability(name, value);
        }

        public static void AddExecutable(string path)
        {
            appiumOptions.AddAdditionalCapability("app", path);
        }

        public static WindowsDriver<WindowsElement> CreateWindowsDriver(Uri uri)
        {
            return new WindowsDriver<WindowsElement>(uri, appiumOptions);
        }

        public static WindowsDriver<IOSElement> CreateIOSDriver(Uri uri)
        {
            return new WindowsDriver<IOSElement>(uri, appiumOptions);
        }

        public static WindowsDriver<AndroidElement> CreateAndroidDriver(Uri uri)
        {
            return new WindowsDriver<AndroidElement>(uri, appiumOptions);
        }
    }
}
