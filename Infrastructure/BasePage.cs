﻿using SeleniumExtras.PageObjects;
using TBI.SharedLibrary.Logging;

namespace AutomationLibrary.Infrastructure
{
    public class BasePage
    {
        protected WebDriverFacade WebDriver { get; set; }
        protected ISerilogLogger Logger { get; set; }

        public BasePage(WebDriverFacade webDriver, ISerilogLogger logger)
        {
            this.WebDriver = webDriver;
            this.Logger = logger;

            PageFactory.InitElements(WebDriver.GetDriver(), this);
        }
    }
}