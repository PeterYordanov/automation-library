﻿using OpenQA.Selenium.Appium.Windows;
using System;
using AutomationLibrary.Enumerations.WinAppDriver;
using AutomationLibrary.Extensions;

namespace AutomationLibrary.Infrastructure
{
    public class WindowsDriverFacade
    {
        private readonly WindowsDriver<WindowsElement> windowsDriver;

        public WindowsDriverFacade(Uri uri)
        {
            windowsDriver = WinAppDriverContext.CreateWindowsDriver(uri);
            windowsDriver.LaunchApp(); //Has to be launched twice, idk why
        }

        public WindowsElement FindElement(string selector, By by)
        {
            return by switch
            {
                By.AccessibilityId => windowsDriver.FindElementByAccessibilityId(selector),
                By.ClassName => windowsDriver.FindElementByClassName(selector),
                By.TagName => windowsDriver.FindElementByTagName(selector),
                By.XPath => windowsDriver.FindElementByXPath(selector),
                By.Id => windowsDriver.FindElementById(selector),
                By.WindowsUIAutomation => windowsDriver.FindElementByWindowsUIAutomation(selector),
                By.CssSelector => windowsDriver.FindElementByCssSelector(selector),
                By.LinkText => windowsDriver.FindElementByLinkText(selector),
                By.PartialLinkText => windowsDriver.FindElementByPartialLinkText(selector),
                By.Name => windowsDriver.FindElementByName(selector),
                By.AbsoluteXPath => windowsDriver.FindElementByAbsoluteXPath(selector),
                _ => throw new Exception("No such element type"),
            };
        }

        public void GoToUrl(string url)
        {
            //WindowsElement searchBar = windowsDriver.FindElementByAbsoluteXPath("/Pane[@ClassName=\"#32769\"][@Name=\"Desktop 1\"]/Window[@ClassName=\"MozillaWindowClass\"][@Name=\"Google - Mozilla Firefox\"]/ToolBar[@Name=\"Navigation\"][@AutomationId=\"nav-bar\"]/ComboBox/Edit[@Name=\"Search with Google or enter address\"][@AutomationId=\"urlbar-input\"]");
            //searchBar.SendKeys(url);
            //searchBar.SendKeys("{ENTER}");
            windowsDriver.Url = url;
        }
    }
}
